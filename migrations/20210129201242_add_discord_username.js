
exports.up = function(knex) {
  return knex.schema.table('hacker_2021', (table) => {
    table.string('discord_username').comment('verified discord username');
  });
};
  
exports.down = function(knex) {
  return knex.schema.table('hacker_2021', (table) => {
    table.dropColumn('discord_username');
  });
};
  