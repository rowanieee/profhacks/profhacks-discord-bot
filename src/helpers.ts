import { Client, TextChannel, DMChannel, Message, User } from 'discord.js';

export function getEmailFromMessage(email: string): string | undefined {
  // See: https://ihateregex.io/expr/email/
  let re = new RegExp('[^@ \t\r\n]+@[^@ \t\r\n]+.[^@ \t\r\n]+');
  let messageParts = email.split(' ');
  let foundEmail: string | undefined;
  messageParts.forEach((element) => {
    if (re.test(email)) {
      foundEmail = element;
      console.log(`Received email: ${foundEmail}`);
    }
  });

  return foundEmail;
}

export function isUserDm(message: Message, client: Client): boolean {
  return (
    !message.guild &&
    message.channel instanceof DMChannel &&
    !(message.author === client.user)
  );
}

export function isMessageInChannel(message: Message, channelName: String): boolean {
  if (!(message.guild && message.channel instanceof TextChannel)) return false;
  return message.channel.name === channelName;
}
