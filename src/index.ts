import process from 'process';
import { Client } from 'discord.js';
import { help, startVerify, email, giveRole, purgeChannel } from './commands';
import { isUserDm, isMessageInChannel } from './helpers';
import { isHackerInDatabase } from './database';

const client = new Client();

client.on('ready', async () => {
  console.log(`Just logged in as ${client.user?.tag}!`);
});

client.on('message', async (message) => {
  // TODO: Load check-in channel settings from file
  if (purgeChannel(message)) return;

  if (isMessageInChannel(message, "check-in")) {
    let deletedMessage = await message.delete({ reason: 'Verification message contains personal information' });
    let hackerEmail = email(message);
    if (hackerEmail) {
      let hacker = await isHackerInDatabase(hackerEmail, message.author.tag);
      if (!hacker) {
        message.author.send(`Email ${hackerEmail} didn't exist in the registration database.`);
        return;
      }

      if (typeof hacker === "string") {
        message.author.send(`The email address (${hackerEmail}) has already been verified! If you think there has been a mistake, please message a ProfHacks server moderator or admin.`);
        return;
      }

      message.author.send(
        `Thank you for verifying your registration, ${hacker.first_name} ${hacker.last_name}!`
      );

      let givenRole = await giveRole(message.guild, message.author)
      if(!givenRole) {
        message.author.send('Failed to give role. Please let the server admins know that the bot can\'t give roles.')
      }
    }
  }

  if (help(message)) return;

  if (startVerify(message)) return;
});

client.login(process.env.DISCORD_TOKEN);
