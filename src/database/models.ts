type SchoolingLevel = 'Middle School' | 'High School' | 'College/University';
export enum ShirtSize {
  XXS = 'XXS',
  XS = 'XS',
  S = 'S',
  M = 'M',
  L = 'L',
  XL = 'XL',
  XL2 = '2XL',
  XL3 = '3XL',
}

export interface User {
  first_name: String;
  last_name: String;
}

interface DetailedUser extends User {
  phone: string;
  email: string;
  gender?: string;
  race?: string;
}

interface Location {
  shipping_address: string;
  country: string;
  state: string;
}

interface School {
  school: string;
  los: SchoolingLevel;
  graduation: number;
  major?: string;
  accepted_student: boolean;
  accepted_student_half_full?: boolean;
}

export interface Hacker extends DetailedUser, Location, School {
  birthdate: Date;
  ieee_member_id?: string;
  shirt_size: ShirtSize;
  accepted_student: boolean;
  accepted_student_half_full?: boolean;
  minor: boolean;
  first_timer: boolean;
  mlh_coc: boolean;
  mlh_ctac_pp: boolean;
  discord_username: string;
}
