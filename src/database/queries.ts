import process from 'process';
import Knex from 'knex';

const knexConfig = require('../../knexfile');

import { User } from './models';

const knex = Knex(knexConfig[process.env.NODE_ENV ?? 'development']);

export async function isHackerInDatabase(
  email: string,
  discordUsername: string
): Promise<User | string | undefined> {
  const hacker = await knex
    .select('first_name', 'last_name', 'discord_username')
    .from<User>(process.env.HACKER_TABLE ?? 'hacker')
    .where('email', email)
    .first();

  if (!hacker) {
    return;
  }

  if (hacker.discord_username) {
    return hacker.discord_username;
  }

  // Add the hacker's discord username to the database
  await knex
    .from(process.env.HACKER_TABLE ?? 'hacker')
    .where('email', email)
    .update('discord_username', discordUsername)

  return {
    first_name: hacker.first_name,
    last_name: hacker.last_name,
  };
}
