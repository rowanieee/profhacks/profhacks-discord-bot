import { Message } from 'discord.js';
import { parseCommand } from './helpers';

export function help(message: Message): boolean {
  let userInput = parseCommand(message.content);
  if (!(userInput && userInput.command == 'help')) {
    return false;
  }
  message.author.send(`Available commands (prefix: !):
  Admin Commands: [purge _n_]
  General Commands: [help, verify]
  #check-in Commands: [email _address_]`);

  return true;
}
