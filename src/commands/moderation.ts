import { Guild, Client, User, Message, TextChannel } from 'discord.js';
import { parseCommand } from './helpers';

export const HACKER_ROLE = 'Hacker';

export function purgeChannel(message: Message): boolean {
  let userInput = parseCommand(message.content);
  if (
    !(message.channel instanceof TextChannel && userInput?.command === 'purge')
  ) {
    return false;
  }

  let numMessages = 99;
  if (userInput?.args.length == 1) {
    let _numMessages = parseInt(userInput?.args[0]);
    if (Number.isNaN(_numMessages) || _numMessages < 0 || _numMessages >= 100) {
      console.log(`Improper usage of ${userInput?.command}`);
      return false;
    }
    numMessages = _numMessages;
  }

  console.log(`Channel ${message.channel} will be purged`);
  message.channel.bulkDelete(numMessages + 1);
  return true;
}

export async function giveRole(
  guild: Guild | null,
  user: User,
  role_string?: String
): Promise<boolean> {
  let hackerRole = guild?.roles.cache.find((role) => role.name === 'Hacker');
  let member = guild?.member(user);
  if (member && hackerRole) {
    try {
      let res = await member.roles.add(hackerRole, 'Email exists in database');
    } catch (error) {
      console.log(`Couldn't give role: ${hackerRole.name}, Reason: ${error}`);
      return false;
    }
    return true;
  }

  return false;
}

export function setNickname(): void {}
