export interface Command {
  command: string;
  args: string[];
}

export function parseCommand(content: string): Command | undefined {
  if (!content.startsWith('!')) return;

  let data = content.slice(1).trim().split(' ');

  return {
    command: data[0].toLowerCase(),
    args: data.length > 1 ? data.slice(1) : [],
  };
}
