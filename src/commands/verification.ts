import { Message } from 'discord.js';
import { parseCommand } from './helpers';

import { getEmailFromMessage } from '../helpers';

export function startVerify(message: Message): boolean {
  let userInput = parseCommand(message.content);
  if (userInput && userInput.command === 'verify') {
    message.author.send(
      'Thank you for joining the Profhacks Discord server!\n' +
        'Please enter your email into the #check-in channel to verify your registration.\n' +
        'Submit your email address using the following command: !email _address_\n'
    );
    return true;
  }
  return false;
}

export function email(message: Message): string | undefined {
  let userInput = parseCommand(message.content);
  if (
    !(userInput && userInput.command == 'email' && userInput.args.length == 1)
  ) {
    message.author.send(
      'Please send the email address in the format of: !email _address_'
    );
    return;
  }

  let email = getEmailFromMessage(userInput.args[0]);
  if (!email) {
    message.author.send(
      'Email not detected. Please send me a properly-formatted email address.'
    );
    return;
  }

  message.author.send(
    `Checking if the email (${email}) exists in the registration database!`
  );

  return email;
}
