const { series, src, dest, watch } = require('gulp');
const ts = require('gulp-typescript');
const nodemon = require('gulp-nodemon');
const path = require('path');
const del = require('del');
const tsProject = ts.createProject('tsconfig.json');

const OUTPUT_DIR = path.resolve(__dirname, 'dist');

function clean(cb) {
  del(OUTPUT_DIR).then(() => cb());
}

function watchFiles(cb) {
  watch('src/**/*.ts', compile);
  cb();
}

function startDevServer(cb) {
  nodemon({
    script: 'dist/index.js',
    env: { NODE_ENV: 'development' },
  });
  cb();
}

function compile(cb) {
  return src('src/**/*.ts').pipe(tsProject()).js.pipe(dest('dist'));
}

const build = series(clean, compile);

exports.default = build;
exports.build = build;
exports.clean = clean;
exports.watch = series(build, startDevServer, watchFiles);
