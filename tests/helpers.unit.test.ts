import { getEmailFromMessage } from '../src/helpers';

describe('email tests', () => {
  it('valid email', () => {
    expect(getEmailFromMessage('twschofield99@gmail.com')).toEqual(
      'twschofield99@gmail.com'
    );
  });

  it('invalid email', () => {
    expect(getEmailFromMessage('asdfjoisafs@oof')).toBeFalsy();
  });

  it('just the email service', () => {
    expect(getEmailFromMessage('@gmail.com')).toBeFalsy();
  });

  it('just a name', () => {
    expect(getEmailFromMessage('thomas.schofield')).toBeFalsy();
  });
});
