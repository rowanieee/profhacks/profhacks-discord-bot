import Knex from 'knex';
const knexConfig = require('../knexfile');
import process from 'process';

import casual from 'casual';

import { isHackerInDatabase, Hacker, User, ShirtSize } from '../src/database';

const knex = Knex(knexConfig['development']);

let testHacker: Hacker;

beforeAll(async () => {
  await knex.table(process.env.HACKER_TABLE).del();
});

beforeEach(() => {
  testHacker = {
    first_name: casual.first_name,
    last_name: casual.last_name,
    phone: casual.phone,
    email: casual.email,
    shipping_address: casual.address,
    school: 'Rowan University',
    birthdate: new Date(casual.date('YYYY-MM-DDTHH:MM:SS.SSSZ')),
    los: 'High School',
    graduation: casual.year,
    country: casual.country,
    state: casual.state,
    shirt_size: ShirtSize.L,
    accepted_student: casual.boolean,
    minor: casual.boolean,
    first_timer: casual.boolean,
    mlh_coc: true,
    mlh_ctac_pp: true,
  };
});

afterEach(async () => {
  await knex.destroy();
});

describe('database integration tests', () => {
  it('read test hacker', async () => {
    await knex.table(process.env.HACKER_TABLE).insert(testHacker);

    let expectedUser: User = {
      first_name: testHacker.first_name,
      last_name: testHacker.last_name,
    };

    const data = await isHackerInDatabase(testHacker.email);
    expect(data).toEqual(expectedUser);
  });

  it('read non-existing user', async () => {
    const data = await isHackerInDatabase('');
    expect(data).toBeUndefined();
  });
});
