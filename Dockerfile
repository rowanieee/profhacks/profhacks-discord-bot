# CONTAINER: Bot Build environment
FROM node:buster as builder

WORKDIR /discord/bot

COPY package.json package-lock.json tsconfig.json gulpfile.js knexfile.js /discord/bot/

RUN npm ci

COPY ./src/ /discord/bot/src/

RUN npm run build
RUN npm prune --production

# CONTAINER: Bot Runtime
FROM node:buster as runtime

WORKDIR /discord/bot
ENV NODE_ENV=production

COPY --from=builder /discord/bot/dist/ /discord/bot/dist/
COPY --from=builder /discord/bot/node_modules/ /discord/bot/node_modules/
COPY --from=builder /discord/bot/package.json /discord/bot/package.json
COPY --from=builder /discord/bot/knexfile.js /discord/bot/knexfile.js

CMD ["npm", "start"]
