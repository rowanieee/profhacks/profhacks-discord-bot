const { env } = require('process');

module.exports = {
  development: {
    client: 'sqlite3',
    connection: {
      filename: './dev.profhacks.db',
    },
    useNullAsDefault: true,
  },

  staging: {
    client: 'postgresql',
    connection: {
      user: 'prof',
      host: 'localhost',
      database: 'prof',
      password: 'hoot',
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },

  production: {
    client: 'postgresql',
    connection: {
      user: env.PGUSER,
      host: env.PGHOST,
      database: env.PGDATABASE,
      password: env.PSPASSWORD,
      port: env.PGPORT,
      ssl: {
        rejectUnauthorized: false,
      },
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },
};
